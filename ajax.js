(function ($) {

  $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
  $('#btnAjax').click(function () { callRestAPI() });
  $('#btnAjaxE').click(function () { callRest() });

  // Perform an asynchronous HTTP (Ajax) API request.
  function callRestAPI() {
    var root = 'https://jsonplaceholder.typicode.com';
    $.ajax({
      url: root + '/photos/34',
      method: 'GET'
    }).then(function (response) {
      console.log(response);
      $('#showResult').html(response.url);
    });
  }
    function callRest() {
    var root = 'https://jsonplaceholder.typicode.com';
    $.ajax({
      url: root + '/albums/99',
      method: 'GET'
    }).then(function (response) {
      console.log(response);
      $('#showResult').html(response.title);
    });
  }
})($);